<?php

namespace Davek1312\Testing;

class TestCase extends \PHPUnit_Framework_TestCase {

    public function setUp() {
        parent::setUp();
        if($this->makesDatabaseMigrations()) {
            $this->setUpDatabaseMigrations();
        }
        if($this->makesDatabaseTransactions()) {
            $this->setUpDatabaseTransactions();
        }
    }

    public function tearDown() {
        parent::tearDown();
        if($this->makesDatabaseMigrations()) {
            $this->tearDownDatabaseMigrations();
        }
        if($this->makesDatabaseTransactions()) {
            $this->tearDownDatabaseTransactions();
        }
    }

    /**
     * @return boolean
     */
    private function makesDatabaseMigrations() {
        return in_array('Davek1312\Database\Testing\MakesDatabaseMigrations', $this->getClassUses());
    }

    /**
     * @return boolean
     */
    private function makesDatabaseTransactions() {
        return in_array('Davek1312\Database\Testing\MakesDatabaseTransactions', $this->getClassUses());
    }

    /**
     * Returns all traits used by a class, its subclasses and trait of their traits.
     *
     * @return array
     */
    private function getClassUses() {
        return array_flip(class_uses_recursive(static::class));
    }
}