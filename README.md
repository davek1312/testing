# davek1312/testing

Testing helpers.

# Installation

The package is available on [Packagist](https://packagist.org/packages/davek1312/testing),
you can install it using [Composer](https://getcomposer.org/).

```bash
composer require davek1312/testing
```

# Usage

Your test class should implement our `TestClass` class:
```php
<?php

namespace Davek1312\Testing;

class YourTestCase extends \Davek1312\Testing\TestCase {

}
```

## Database
Resetting The Database After Each Test
It is often useful to reset your database after each test so that data from a previous test does not interfere with subsequent tests.

### Using Migrations
One option is to rollback the database after each test and migrate it before the next test:
```php
<?php

namespace Davek1312\Testing;

class YourTestCase extends \Davek1312\Testing\TestCase {
    
    use \Davek1312\Database\Testing\MakesDatabaseMigrations;
    
}
```

### Using Transactions
Another option is to wrap every test case in a database transaction:
```php
<?php

namespace Davek1312\Testing;

class YourTestCase extends \Davek1312\Testing\TestCase {
    
    use \Davek1312\Database\Testing\MakesDatabaseTransactions;
    
}
```